<?="<?php //generated at ".date("Y-m-d H:i:s")?>

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{{$h->tblUper}};
use Illuminate\Support\Facades\Auth;

class {{$h->tblUper}}Controller extends Controller
{
    /**
     * Display  listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view("{{$h->tblLower}}.{{$h->tblLower}}_frm");
    }

    public function list(Request $req){
        // dd($req);
        ${{$h->tblLower}} = {{$h->tblUper}}::latest()->where('id','LIKE',"%$req->q%")->paginate(5);
        header('Content-Type: application/json');
        echo json_encode(compact(['{{$h->tblLower}}']));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function store(Request $request)
    {
        $request = json_decode(file_get_contents('php://input'));
        $h=$request->h;
        $f=$request->f;
        $arr = (array) $h;

        
        if($f->crud=="c"){
            unset($arr['{{$h->pk}}']);
            @if ($h->timestamps==1)
            $arr['created_at']=date('Y-m-d H:i:s');
            //$arr['created_by']=Auth::user()->userid;
            @endif
            $data=new {{$h->tblUper}};
            $data->create($arr);
        }else{
            @if ($h->timestamps==1)
            $arr['updated_at']=date('Y-m-d H:i:s');
            //$arr['updated_by']=Auth::user()->userid;
            @endif
            $data={{$h->tblUper}}::find($arr['{{$h->pk}}']);
            $data->update($arr);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $h={{$h->tblUper}}::find($id);
        header('Content-Type: application/json');
        echo json_encode(compact(['h']));
    }

    public function delete($id)
    {
        $data = {{$h->tblUper}}::find($id);
        $data->delete();
    }   
}