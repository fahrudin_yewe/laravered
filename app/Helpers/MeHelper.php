<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class MeHelper
{

    public static function get_select2($q)
    {
        $data = \App\Models\Group::where('name', 'like', "%$q%")->get();
        $res  = [];
        foreach ($data as $v) {
            $res[] = ["id" => $v->id, "text" => $v->name];
        }
        return response()->json($res);
    }

    public static function parsys($key)
    {
        $data = DB::table('parsys')->where('name', $key)->first();
        echo empty($data) ? '' : $data->value;
    }
}