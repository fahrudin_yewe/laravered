<?php //generated at 2021-02-08 12:48:24
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MasterAccess;
use Illuminate\Support\Facades\Auth;

class MasterAccessController extends Controller
{
    /**
     * Display  listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {   
        return view("master_access.master_access_frm");
    }

    public function list(Request $req){
        // dd($req);
        $master_access = MasterAccess::latest()->where('id','LIKE',"%$req->q%")->paginate(5);
        header('Content-Type: application/json');
        echo json_encode(compact(['master_access']));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
      public function store(Request $request)
    {
        $request = json_decode(file_get_contents('php://input'));
        $h=$request->h;
        $f=$request->f;
        $arr = (array) $h;

        
        if($f->crud=="c"){
            unset($arr['id']);
                        $data=new MasterAccess;
            $data->create($arr);
        }else{
                        $data=MasterAccess::find($arr['id']);
            $data->update($arr);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        $h=MasterAccess::find($id);
        header('Content-Type: application/json');
        echo json_encode(compact(['h']));
    }

    public function delete($id)
    {
        $data = MasterAccess::find($id);
        $data->delete();
    }   
}