<?php

namespace App\Http\Controllers;

use App\Models\Parsys;
use Illuminate\Http\Request;

class ParsysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("parsys.parsys_frm");
    }

    function list() {
        $parsys = Parsys::first()->orderBy('id')->paginate(5);
        header('Content-Type: application/json');
        echo json_encode(compact(['parsys']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = json_decode(file_get_contents('php://input'));
        $h       = $request->h;
        $f       = $request->f;

        $arr = array_merge((array) $h);
        // dd($arr);
        if ($f->crud == "c") {
            $data = new Parsys;
            $data->create($arr);
        } else {
            $data = Parsys::find($h->id);
            $data->update($arr);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $h = Parsys::find($id);
        header('Content-Type: application/json');
        echo json_encode(compact(['h']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        $data = Parsys::find($id);
        $data->delete();
    }
}