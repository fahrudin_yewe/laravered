<?php //generated at 2021-02-08 13:47:03
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    /**
     * Display  listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {   
        return view("users.users_frm");
    }

    public function list(Request $req){
        // dd($req);
        $users = Users::latest()->where('id','LIKE',"%$req->q%")->paginate(5);
        header('Content-Type: application/json');
        echo json_encode(compact(['users']));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
      public function store(Request $request)
    {
        $request = json_decode(file_get_contents('php://input'));
        $h=$request->h;
        $f=$request->f;
        $arr = (array) $h;

        
        if($f->crud=="c"){
            unset($arr['id']);
                        $arr['created_at']=date('Y-m-d H:i:s');
            //$arr['created_by']=Auth::user()->userid;
                        $data=new Users;
            $data->create($arr);
        }else{
                        $arr['updated_at']=date('Y-m-d H:i:s');
            //$arr['updated_by']=Auth::user()->userid;
                        $data=Users::find($arr['id']);
            $data->update($arr);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        $h=Users::find($id);
        header('Content-Type: application/json');
        echo json_encode(compact(['h']));
    }

    public function delete($id)
    {
        $data = Users::find($id);
        $data->delete();
    }   
}