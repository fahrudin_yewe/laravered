<?php //generated at 2021-02-18 19:18:44
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Link;
use Illuminate\Support\Facades\Auth;

class LinkController extends Controller
{
    /**
     * Display  listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {   
        return view("link.link_frm");
    }

    public function list(Request $req){
        // dd($req);
        $link = Link::latest()->where('id','LIKE',"%$req->q%")->paginate(5);
        header('Content-Type: application/json');
        echo json_encode(compact(['link']));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
      public function store(Request $request)
    {
        $request = json_decode(file_get_contents('php://input'));
        $h=$request->h;
        $f=$request->f;
        $arr = (array) $h;

        
        if($f->crud=="c"){
            unset($arr['id']);
                        $arr['created_at']=date('Y-m-d H:i:s');
            //$arr['created_by']=Auth::user()->userid;
                        $data=new Link;
            $data->create($arr);
        }else{
                        $arr['updated_at']=date('Y-m-d H:i:s');
            //$arr['updated_by']=Auth::user()->userid;
                        $data=Link::find($arr['id']);
            $data->update($arr);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        $h=Link::find($id);
        header('Content-Type: application/json');
        echo json_encode(compact(['h']));
    }

    public function delete($id)
    {
        $data = Link::find($id);
        $data->delete();
    }   
}