<?php //generated at 2021-02-18 20:23:03
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use Illuminate\Support\Facades\Auth;

class MenuController extends Controller
{
    /**
     * Display  listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {   
        return view("menu.menu_frm");
    }

    public function list(Request $req){
        // dd($req);
        $menu = Menu::latest()->where('id','LIKE',"%$req->q%")->paginate(5);
        header('Content-Type: application/json');
        echo json_encode(compact(['menu']));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
      public function store(Request $request)
    {
        $request = json_decode(file_get_contents('php://input'));
        $h=$request->h;
        $f=$request->f;
        $arr = (array) $h;

        
        if($f->crud=="c"){
            unset($arr['id']);
                        $arr['created_at']=date('Y-m-d H:i:s');
            //$arr['created_by']=Auth::user()->userid;
                        $data=new Menu;
            $data->create($arr);
        }else{
                        $arr['updated_at']=date('Y-m-d H:i:s');
            //$arr['updated_by']=Auth::user()->userid;
                        $data=Menu::find($arr['id']);
            $data->update($arr);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        $h=Menu::find($id);
        header('Content-Type: application/json');
        echo json_encode(compact(['h']));
    }

    public function delete($id)
    {
        $data = Menu::find($id);
        $data->delete();
    }   
}