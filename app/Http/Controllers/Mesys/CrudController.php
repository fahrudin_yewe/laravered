<?php

namespace App\Http\Controllers\Mesys;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CrudController extends Controller
{

    public function index()
    {
        return view("mesys/crud/crud_frm");
    }

    function fn($fn, Request $request) {
        return $this->$fn($request);
    }

    public function getDatabases(Request $request)
    {
        $data_dbs  = DB::select("SELECT table_schema FROM information_schema.TABLES GROUP BY table_schema");
        $databases = [];
        foreach ($data_dbs as $key => $v) {
            $databases[] = $v->table_schema;
        }
        return response()->json(compact(['databases']));
    }

    public function getTables(Request $request)
    {
        $h = json_decode($request->h);
        // print_r($h);die();
        $data_dbs = DB::select("SELECT table_name FROM information_schema.TABLES a WHERE table_schema=? AND table_type=? GROUP BY table_name", [$h->db, 'BASE TABLE']);
        $tables   = [];
        foreach ($data_dbs as $key => $v) {
            $tables[] = $v->table_name;
        }
        return response()->json(compact(['tables']));
    }

    private function getFields($request)
    {
        $h = json_decode($request->h);
        // dd($h);
        try {
            $data_dbs = DB::select("SELECT * FROM information_schema.COLUMNS a WHERE table_schema=? AND table_name=?", [$h->db, $h->tbl]);
            // dd($data_dbs);
            $fields = [];
            foreach ($data_dbs as $key => $v) {
                if (in_array($v->COLUMN_NAME, ['created_by', 'updated_by', 'created_at', 'updated_at', 'deleted_at'])) {
                    $v->ELEMENT = "none";
                } elseif (in_array($v->EXTRA, ['auto_increment'])) {
                    $v->ELEMENT = "text readonly";
                } else {
                    $v->ELEMENT = "text";
                }
                $v->CAPTION  = ucwords(str_replace('_', ' ', $v->COLUMN_NAME));
                $v->REQUIRED = "";
                $fields[]    = $v;
            }
            return $fields;
        } catch (\Exception $e) {
            return [];
        }
    }

    private function getPk($h)
    {
        $pk = DB::select("SELECT column_name FROM information_schema.COLUMNS a WHERE table_schema=? AND table_name=? AND extra=?", [$h->db, $h->tbl, 'auto_increment']);
        return $pk[0]->column_name;
    }

    public function previewScript(Request $request)
    {
        $h     = json_decode($request->h);
        $h->pk = $this->getPk($h);

        $h->tblUper        = str_replace('_', '', ucwords($h->tbl, "_"));
        $h->tblLower       = strtolower($h->tbl);
        $script_route      = htmlspecialchars(view("mesys.crud.crud_template_route", compact(['h'])));
        $script_controller = htmlspecialchars(view("mesys.crud.crud_template_controller", compact(['h'])));
        $dataFields        = $this->getFields($request);
        $script_model      = htmlspecialchars(view("mesys.crud.crud_template_model", compact(['h', 'dataFields'])));
        $script_view       = htmlspecialchars(view("mesys.crud.crud_template_view", compact(['h', 'dataFields'])));
        $path_route        = "\\routes\\web_part\\" . $h->tbl . ".php";
        $path_controller   = "\\app\\Http\\Controllers\\" . $h->tblUper . "Controller.php";
        $path_model        = "\\app\\Models\\" . $h->tblUper . ".php";
        $path_view         = "\\resources\\views\\" . $h->tbl . "\\" . $h->tbl . "_frm.blade.php";
        return response()->json(compact(['script_route', 'script_controller', 'script_model', 'script_view', 'path_route', 'path_controller', 'path_model', 'path_view']));
    }

    public function writeScript(Request $request)
    {
        $h    = $request->h;
        $path = base_path() . $request->path;
        if (file_exists($path)) {
            return response()->json("File $request->id sudah ada sebelumnya", 404);
        } else {
            if (!file_exists(dirname($path))) {
                mkdir(dirname($path), 0755, true);
            }
            $myfile = fopen($path, "w") or die("Gagal memuat file!");
            fwrite($myfile, html_entity_decode($request->script));
            fclose($myfile);
            $msg = "File $path berhasil dibuat!";
            return response()->json(compact(['msg']));
        }
    }

    public function delFileExist(Request $request)
    {
        $path = base_path() . $request->path;
        if (file_exists($path)) {
            unlink($path);
            $msg = "File $path berhasil dihapus!";
            return response()->json(compact(['msg']));
        } else {
            return response()->json('File tidak ditemukan', 404);
        }

    }

}