<?php //generated at 2021-02-18 20:23:03
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model {
	
		use SoftDeletes;
	
	protected $connection = 'mysql';
	public $incrementing = 1;
	public $timestamps = 1;
	protected $hidden = [];
	//protected $dates = ['deleted_at'];
	protected $table = 'menu';
	protected $primaryKey = "id";
	protected $fillable = [
		'id',
		'label',
		'redirect',
		'url',
		'parent',
		'icon',
		'order_no',
		'note',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at',
		'deleted_at',
	];

	public function rel_created_by() {
		return $this->belongsTo('App\Model\User', 'created_by');
	}

}