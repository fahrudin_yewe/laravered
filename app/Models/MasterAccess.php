<?php //generated at 2021-02-08 12:48:24
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterAccess extends Model
{

    protected $connection = 'mysql';
    public $incrementing  = true;
    public $timestamps    = true;
    protected $hidden     = [];
    protected $dates      = ['deleted_at'];
    protected $table      = 'master_access';
    protected $primaryKey = "id";
    protected $fillable   = [
        'id',
        'name',
        'note',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
    ];

    public function rel_created_by()
    {
        return $this->belongsTo('App\Model\User', 'created_by');
    }

}