<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parsys extends Model
{
    use HasFactory;

    protected $primaryKey="id";
    protected $table = "parsys";
    public $timestamps = false;
    protected $fillable = [
        'id',
        'name',
        'value',
        'note',
    ];

}