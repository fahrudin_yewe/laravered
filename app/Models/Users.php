<?php //generated at 2021-02-08 13:47:03
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Users extends Model {
	
		use SoftDeletes;
	
	protected $connection = 'mysql';
	public $incrementing = 1;
	public $timestamps = 1;
	protected $hidden = [];
	//protected $dates = ['deleted_at'];
	protected $table = 'users';
	protected $primaryKey = "id";
	protected $fillable = [
		'id',
		'id_group',
		'name',
		'email',
		'hp',
		'gender',
		'img',
		'email_verified_at',
		'password',
		'remember_token',
		'created_at',
		'updated_at',
	];

	public function rel_created_by() {
		return $this->belongsTo('App\Model\User', 'created_by');
	}

}