<?php //generated at 2021-02-08 12:48:24
use App\Http\Controllers\MasterAccessController;
use Illuminate\Support\Facades\Route;

Route::get('/master_access/list', [MasterAccessController::class, "list"])->name("master_access.list");
Route::get('/master_access/delete/{id}', [MasterAccessController::class, "delete"])->name("master_access.delete");

//route resource harus di paling bawah
Route::resource('/master_access', MasterAccessController::class)->only(['index', 'store', 'show']);