<?php 
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BackendController;
Route::resource('/dashboard',BackendController::class)->only(['index']);
Route::post('/backend/cek',[BackendController::class,"cek"]);