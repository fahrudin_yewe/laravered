<?php //generated at 2021-02-08 13:46:04
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GroupController;

Route::get('/group/list',[GroupController::class,"list"])->name("group.list");
Route::get('/group/delete/{id}',[GroupController::class,"delete"])->name("group.delete");

//route resource harus di paling bawah
Route::resource('/group',GroupController::class)->only(['index','store','show']);