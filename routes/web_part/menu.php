<?php //generated at 2021-02-18 20:23:03
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MenuController;

Route::get('/menu/list',[MenuController::class,"list"])->name("menu.list");
Route::get('/menu/delete/{id}',[MenuController::class,"delete"])->name("menu.delete");

//route resource harus di paling bawah
Route::resource('/menu',MenuController::class)->only(['index','store','show']);