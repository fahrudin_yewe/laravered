<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ParsysController;

Route::get('/parsys/list',[ParsysController::class,"list"])->name("parsys.list");
Route::get('/parsys/delete/{id}',[ParsysController::class,"delete"])->name("parsys.delete");

//route resource harus di paling bawah
Route::resource('/parsys',ParsysController::class)->only(['index','store','show']);