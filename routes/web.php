<?php
use App\Http\Controllers\Mesys\CrudController;
use Illuminate\Support\Facades\Route;

foreach (File::allFiles(__DIR__ . '/web_part') as $route) {
    require_once $route->getPathname();
}

Route::get('/', function ($id = null) {
    // Only executed if {id} is numeric...
    return view('login');
});

Route::resource('/crud', CrudController::class)->only(['index']);
Route::get('/crud/delFileExist', [CrudController::class, "delFileExist"]);
Route::get('/crud/writeScript', [CrudController::class, "writeScript"]);
Route::get('/crud/previewScript', [CrudController::class, "previewScript"]);
Route::get('/crud/getDatabases', [CrudController::class, "getDatabases"]);
Route::get('/crud/getTables', [CrudController::class, "getTables"]);
Route::get('/crud/getFields', [CrudController::class, "getFields"]);
Route::post('/crud/writeScript', [CrudController::class, "writeScript"]);